/**
 * Заказчик
 * name - Наименование
 * address - Адрес
 * phone - Телефон
 * contact - Контактное лицо
 */
class Customer {
    name = null
    address = null
    phone = null
    contact = null
    setName (name) {
        this.name = name
    }
    setAddress (address) {
        this.address = address
    }
    setPhone (phone) {
        this.phone = phone
    }
    setContact (contact) {
        this.contact = contact
    }
    getName () {
        return this.name
    }
    getAddress () {
        return this.address
    }
    getPhone () {
        return this.phone
    }
    getContact () {
        return this.contact
    }
}
/**
 * Товар
 * price - Цена (у.е)
 * delivery - Доставка
 * * false - нету доставки (самовывоз)
 * * true - есть доставка
 * description - Описание
 * TODO: Узанть про выбор типа доставки
 */
class Product {
    price = null
    delivery = false
    description = null
    setPrice (price) {
        this.price = price
    }
    setDelivery (delivery) {
        this.delivery = delivery
    }
    setDescription (description) {
        this.description = description
    }
    getPrice () {
        return this.price
    }
    getDelivery () {
        return this.delivery
    }
    getDescription () {
        return this.description
    }
}

/**
 * Заказ
 * customer - Заказчик
 * * null - нету заказчика
 * * [object Customer] - инстанс класса с информацией о заказчике
 * product - Заказанный товар
 * * null - отсутсвие заказанного товаров
 * * [object Product] - инстанс класса с информацией о товаре
 */
class Order {
    customer = null
    product = null
    quantity = null
    date = null
    setCustomer (customer) {
        this.customer = customer
    }
    setProduct (product) {
        this.product = product
    }
    setQuantity (quantity) {
        this.quantity = quantity
    }
    setDate (date) {
        this.date = date
    }
    getCustomer () {
        return this.customer
    }
    getProduct () {
        return this.product
    }
    getDate () {
        return this.date
    }
}

exports.Customer = Customer
exports.Product = Product
exports.Order = Order