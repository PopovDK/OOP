const { Customer, Product, Order } = require('./entities');

const customer = new Customer()
customer.setName('ООО Заря')
customer.setAddress('Москва ул. Набережная д. 11 кв. 10')
customer.getPhone('+79432123456')
customer.setContact('Дим Димыч')

const product = new Product()
product.setPrice(1000)
product.setDescription('Очень нужный товар')
product.setDelivery(true)

const order = new Order ()
order.setCustomer(customer)
order.setProduct(product)
order.setQuantity(2)
order.setDate(new Date())

console.log(order)