const data = require('./data.json')
const { Customer, Product, Order } = require('./entities');

const customers = data.customers.map(customer => {
    const customerInstance = new Customer()
    customerInstance.setName(customer.name)
    customerInstance.setPhone(customer.phone)
    customerInstance.setContact(customer.contact)
    customerInstance.setAddress(customer.address)
    return {
        id: customer.id,
        instance: customerInstance
    }
})

const products = data.products.map(product => {
    const productInstance = new Product()
    productInstance.setPrice(product.price)
    productInstance.setDescription(product.description)
    productInstance.setDelivery(product.delivery)
    return {
        id: product.id,
        instance: productInstance
    }
})

const orders = data.orders.map(order => {
    const orderCustomer = customers.find(customer => customer.id === order.customerId)
    const orderProduct = products.find(product => product.id === order.productId)
    const orderInstance = new Order()

    if (orderCustomer && orderCustomer.instance) {
        orderInstance.setCustomer(orderCustomer.instance)
    }

    if (orderProduct && orderProduct.instance) {
        orderInstance.setProduct(orderProduct.instance)
    }

    orderInstance.setQuantity(order.quantity)
    orderInstance.setDate(new Date(order.date))
    return orderInstance
})

console.log(orders)
