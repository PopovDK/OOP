const { Model, DataTypes } = require('sequelize')

class User extends Model {}

User.init({
    name: DataTypes.STRING,
    city: DataTypes.STRING
}, { modelName: 'user' })

exports.User = User
