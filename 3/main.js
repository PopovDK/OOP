const fs = require('fs')
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:');
const data = require('./data.json')

const Customer = sequelize.define('customer', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    phone: DataTypes.STRING,
    contact: DataTypes.STRING
}, {
    createdAt: false,
    updatedAt: false
})

const Product = sequelize.define('product',{
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    price: DataTypes.NUMBER,
    delivery: DataTypes.BOOLEAN,
    description: DataTypes.STRING
}, {
    createdAt: false,
    updatedAt: false
})


const Order = sequelize.define('order',{
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    quantity: DataTypes.NUMBER,
    date: DataTypes.DATE,
}, {
    createdAt: false,
    updatedAt: false
})

Order.belongsTo(Customer)
Order.belongsTo(Product)


;(async () => {
    await sequelize.sync();

    for (const customer of data.customers) {
        await Customer.create({
            id: customer.id,
            name: customer.name,
            phone: customer.phone,
            contact: customer.contact,
            address: customer.address
        })
    }

    for (const product of data.products) {
        await Product.create({
            id: product.id,
            price: product.price,
            delivery: product.delivery,
            description: product.description
        })
    }

    for (const order of data.orders) {
        await Order.create({
            id: order.id,
            customerId: order.customerId,
            productId: order.productId,
            quantity: order.quantity,
            date: new Date(order.date)
        })
    }

    const customers = await Customer.findAll()

    const products = await Product.findAll()
    const orders = await Order.findAll()
    const json = JSON.stringify({
        customers,
        products,
        orders
    }, null, 2)

    fs.writeFile('newData.json', json, () => {})

})()
